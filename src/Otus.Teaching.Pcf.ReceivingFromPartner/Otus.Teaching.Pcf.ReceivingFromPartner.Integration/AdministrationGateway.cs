﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Transports;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Shared.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly HttpClient _httpClient;
        private readonly IPublishEndpoint publishEndpoint;

        public AdministrationGateway(HttpClient httpClient, IPublishEndpoint publishEndpoint)
        {
            _httpClient = httpClient;
            this.publishEndpoint = publishEndpoint;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            //var response = await _httpClient.PostAsync($"api/v1/employees/{partnerManagerId}/appliedPromocodes", 
            //    new StringContent(string.Empty));


            var dto = new NotifyAdminAboutPartnerManagerPromoDto()
            {
                PartnerManagerId = partnerManagerId
            };

            await publishEndpoint.Publish(dto);

//            response.EnsureSuccessStatusCode();
        }
    }
}