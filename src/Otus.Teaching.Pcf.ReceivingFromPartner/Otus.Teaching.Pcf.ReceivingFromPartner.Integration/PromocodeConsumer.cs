﻿using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PromocodeConsumer : IConsumer<BusReceivingPromoCodeRequest>
    {
        private readonly PromocodeService promocodeService;

        public PromocodeConsumer(PromocodeService promocodeService)
        {
            this.promocodeService = promocodeService;
        }

        public async Task Consume(ConsumeContext<BusReceivingPromoCodeRequest> context)
        {
            await promocodeService.ReceivePromoCodeFromPartnerWithPreferenceAsync(context.Message.Id, context.Message.Request);
        }
    }
}
