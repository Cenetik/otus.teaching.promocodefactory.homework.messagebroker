﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core;
using Shared.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class AdministationPromocodesConsumer : IConsumer<NotifyAdminAboutPartnerManagerPromoDto>
    {
        private readonly AdministrationPromocodeService service;

        public AdministationPromocodesConsumer(AdministrationPromocodeService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<NotifyAdminAboutPartnerManagerPromoDto> context)
        {
            await service.UpdateApplliedPromocode(context.Message.PartnerManagerId);
        }
    }   
}
